$(document).on('bodyContLoaded', function() {

	// Data
	var projectData = projectData || {};


	projectData = {
		'panels':{

			'tele_enroll':{
				'EnrollmentLookUp1': 'collapsed',
				'ParticipantDetails1': 'expanded',
				'openEnrollment1':'collapsed',
				'EligiblePlans1': 'collapsed',
				'ActivePlans1': 'collapsed'
			}

		},

		'lookupForm1':{
			'ssn' : '',
			'ssn1' : '',
			'ssn2' : '',
			'pin_npin' : '',
			'tiaa_cref_number' : '',
			'button': {
				'elookup': 'btnOff'
				} 
			}
	 	};


	// $.fn.bindData = function(){var $allElements = this;

	// 	$allElements.each(function(){
			
	// 		var _currentID = $(this).attr('id');

	// 		console.log(_currentID);

			


	// 	});

	// };

	
	function panelChange(){

		$.each(projectData.panels.tele_enroll, function(key, value){

				var __elem = $('#'+key+'');
				var __status = __elem.find('a').attr('class');

				if(__status === value){
					// console.log("Dont Do anything");
				}else{
					setTimeout(function(){
						__elem.trigger('click');
					}, 200);
				}
			});
	};

	function panelTwoWay(){
		$('.panels > .hd').each(function(){
			$(this).find('a').on('click', function(){
				// console.log($(this).parent().attr('data-twoWaybinding'));
			});
		});
	};


	panelChange();
	var dataToUpdate = projectData.panels.tele_enroll;

	// $('#saveBtn').on('click', function(){
	// 	dataToUpdate['ParticipantDetails1'] = 'collapsed';
	// 	dataToUpdate['openEnrollment1'] = 'expanded';
	// 	panelChange();
	// });


	// $('#lookupCancelBtn').on('click', function(e){e.preventDefault();
	// 	dataToUpdate['ParticipantDetails1'] = 'expanded';
	// 	dataToUpdate['openEnrollment1'] = 'expanded';
	// 	panelChange();
	// })

	// $('#EnrollmentLookUp1, #ParticipantDetails1, #openEnrollment1, #EligiblePlans1, #ActivePlans1').bindData();




	var DataUpdate = {
		setData: function(form, elem, val){
			projectData[form][elem] = val;

			// console.log(projectData);
			
			$(document).trigger('dataUpdated', [form, elem, val]);
			return projectData;
		},
		getData: function(data, key){
			return data[key]
		}
	};

	

	// 	CONTROLLER
    var FormModule = {
    	events: function(obj, callback){var _ = this;
    		_.$obj = obj;
			_.$obj.each(function(){var _t = $(this);
				_t.find('input, select, textarea').on('change keyup', function(e){ var _tt = $(this);
					return callback(_t, _tt, e.type);
				});
			});
    	}
    };


    var dataUpdate = Object.create(DataUpdate);

    function updateData(){
		var $form = arguments[0],
			$elem = arguments[1],
			eveName = arguments[2],
			
			formId = $form.attr('id'),
			elemId = $elem.attr('id'),
			key = formId.elemId,
			val = $elem.val();

			if(projectData[formId] === undefined){
				projectData[formId] = {};				
			};

		dataUpdate.setData(formId, elemId, val);
	};

	// Run this function when the page loads
	function runAtStart(container){
		// console.log(projectData[container]);
		$.each(projectData[container], function(key, value){
			if(key !== 'button'){
				$('#'+key+'').val(value);
			}else if(key === 'button'){
				$.each(value, function(key1, value1){
					$('#'+key1+'').addClass(value1);
				})
			}
		})
	};


	// Form1
    var $lookup1      	= $('#lookupForm1');
	var formModule1 	= Object.create(FormModule);
	formModule1.events($lookup1, updateData);	



	// Form2
    var $lookup2      	= $('#lookupForm2');
	var formModule2 	= Object.create(FormModule);
	formModule2.events($lookup2, updateData);
	
	// Form3
    var $partDetail     = $('#defaultform');
	var partDetail 		= Object.create(FormModule);
	partDetail.events($partDetail, updateData);


	// Run at One
	runAtStart('lookupForm1');
	panelTwoWay();


	//VIEW
	$(document).off('dataUpdated').on('dataUpdated', function(e, form, elem, val){
		
		if(form === 'lookupForm1'){
			ssnFocus(form, elem, val);
			enableLookupBtn(form, elem, val);
		};

		// checkAccessCode(arguments)
	});


	// SSN FOCUS
	function ssnFocus(form, elem, val){

		var $elem1 = $('#ssn'),
		$elem2 = $('#ssn1'),
		$elem3 = $('#ssn2');

		if(elem === 'ssn' && val.length === 3){
			$elem2.focus().select();
		};

		if(elem === 'ssn1' && val.length === 2){
			$elem3.focus().select();
		};
	};


	// Button Enabaling
	function enableLookupBtn(form, elem, val){
		var val1 	= projectData.lookupForm1.ssn,
		val2 		= projectData.lookupForm1.ssn1,
		val3 		= projectData.lookupForm1.ssn2,
		$button 	= $('#elookup');

		if(val1 !== '' && val2 !== '' && val3 !== ''){
			$button.removeClass('btnOff');
		}
	};


	// Form Validation 
	var $partDetailsForm = $('#partcipantDetailForm')
		formID = $partDetailsForm.attr('id');

	// Hide all error Messages onload
	$('#'+formID+'Errors').find('li').hide();

	// $('#partcipantDetailForm').udValidate();


	// On validation success
	function truthyForm(id, $parent){
		var _formID = $parent.closest('form').attr('id');

		//Remove the errorClass from the parent
		$parent.removeClass('alertHighlight');
		
		// Hide the error message
		$('#'+id+'-err').hide();

		errorBoxCondition(_formID, true);
	};


	// On validation error
	function falsyForm(id, $parent){
		var _formID = $parent.closest('form').attr('id');

		// Add ErrorClass for the parent
		$parent.addClass('alertHighlight');
		
		// Show Error Message
		$('#'+id+'-err').show();

		errorBoxCondition(_formID, false);
	};


	var isErrorPresent = true;

	// To show or hide the error container
	function errorBoxCondition(formID, flag){

		var $errorContainer = $('#'+formID+'Errors');

		isErrorPresent  = $errorContainer.find('li').is(':visible');

		if(flag){

			if(!isErrorPresent){

				// the element validation is true
				$errorContainer.removeClass('visible');

			};

		}else{
			// the element validation is false
			$errorContainer.addClass('visible');

		}

	};



	$partDetailsForm.submit(function(e){e.preventDefault();

		var $elements = $(this).find('[data-required]');

		$elements.each(function(){  
			var _$elem = $(this),
				_isGroup = _$elem.attr('data-group'),

				_$element = (_isGroup) ? $(this).find('input')  : $(this),

				_val  = _$element.val(),
				_type = _$element.attr('type'),
				_id = _$element.attr('id'),
				_$parent, _eName, _checkFlag;



				console.log(_$element, _val, _type);

				// Finding the parent
				///***************
				if(_$element.closest('.lblFieldPair').length > 0){

					_$parent = _$element.closest('.lblFieldPair');
				}else{
					_$parent = _$element.closest('.lblFieldPairV');
				}	
				

			if(_val === '' || _val === 'none' || _val === undefined){

				falsyForm(_id, _$parent);

			}else{

				truthyForm(_id, _$parent);

			};


			if(_isGroup){
				console.log("its Group", _$element);
			};


			// This is for Radio and checkbox validation
			// if(_type === 'radio' || _type === 'checkbox'){

				// console.log("Radio or Check box",  $($(this)[0]+':radio[name="'+_eName+'"]:checked').length);
				//$(elem[name]:checked)

			// 	_eName = _$element.attr('name');
			// 	_checkFlag = $('input:radio[name="nameRadioButton"]:checked');


				
			// 	console.log("Radio or Check box",  $('input:radio[name="nameRadioButton"]:checked'));
			// 	//$(elem[name]:checked)
			// };

		});

		return false;
	})
	
	
});